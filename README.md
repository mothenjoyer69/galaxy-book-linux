# galaxy-book-linux
Automated script to build a bootable distro for the Samsung Galaxy Book2 (SDM850). This is purely testing right now and 99% of it is done outside of this script. AKA this won't just boot, nor will it function or anything else.

Status:
> Technically boots, display turns off. Likely a DRM driver issue so I've got a couple of ideas.

Todo:
> Fix the display issue, create script to pull firmware during install, add GPU firmware (likely causing the display issue). 
